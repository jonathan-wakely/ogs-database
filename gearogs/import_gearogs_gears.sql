\echo 
\echo Start import gears
\echo ------------------

-- Disabled because database is dropped
--DROP TABLE public.gear_makers;
--DROP TABLE public.gear_links;
--DROP TABLE public.gear_controls;
--DROP TABLE public.gear_connection_descriptions;
--DROP TABLE public.gear_connections;
--DROP TABLE public.gear_indicators;
--DROP TABLE public.gear_specs;
--DROP TABLE public.gear_dimensions;
--DROP TABLE public.gear_channels;
--DROP TABLE public.gear_functions;
--DROP TABLE public.gears;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE gears_import_step1;
DROP TABLE gears_import_step2;
DROP TABLE gears_import_connections;

\echo
\echo Create tables
CREATE TABLE public.gears (
    gear_id integer NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    model_number text COLLATE pg_catalog."default",
    form_factor text COLLATE pg_catalog."default",
    signal_path text COLLATE pg_catalog."default",
    country text COLLATE pg_catalog."default",
    date text COLLATE pg_catalog."default", 
    weight text COLLATE pg_catalog."default", 
    power_source text COLLATE pg_catalog."default", 
    frequency_response text COLLATE pg_catalog."default", 
    dimensions_old text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    CONSTRAINT gears_pkey PRIMARY KEY (gear_id)
) TABLESPACE pg_default;

CREATE TABLE public.gear_functions (
    gear_function_id serial NOT NULL,
    function text COLLATE pg_catalog."default",
    definition text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_functions_pkey PRIMARY KEY (gear_function_id),
    CONSTRAINT gear_functions_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_channels (
    gear_channel_id serial NOT NULL,
    type text COLLATE pg_catalog."default",
    information text COLLATE pg_catalog."default",
    quantity text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_channels_pkey PRIMARY KEY (gear_channel_id),
    CONSTRAINT gear_channels_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_dimensions (
    gear_dimension_id serial NOT NULL,
    width numeric,
    depth numeric,
    height numeric,
    notes text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_dimension_pkey PRIMARY KEY (gear_dimension_id),
    CONSTRAINT gear_dimensions_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_specs (
    gear_spec_id serial NOT NULL,
    name text COLLATE pg_catalog."default",
    value text COLLATE pg_catalog."default",
    information text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_specs_pkey PRIMARY KEY (gear_spec_id),
    CONSTRAINT gear_specs_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_indicators (
    gear_indicator_id serial NOT NULL,
    indication text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    scale text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_indicators_pkey PRIMARY KEY (gear_indicator_id),
    CONSTRAINT gear_indicators_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_connections (
    gear_connection_id serial NOT NULL,
    name text COLLATE pg_catalog."default",
    connector text COLLATE pg_catalog."default",
    description text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_connections_pkey PRIMARY KEY (gear_connection_id),
    CONSTRAINT gear_connections_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_connection_descriptions (
    gear_connection_description_id serial NOT NULL,
    description text COLLATE pg_catalog."default",
    gear_connection_id integer NOT NULL,
    CONSTRAINT gear_connection_descriptions_pkey PRIMARY KEY (gear_connection_description_id),
    CONSTRAINT gear_connections_gear_connection_id_fkey FOREIGN KEY (gear_connection_id)
        REFERENCES public.gear_connections (gear_connection_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_controls (
    gear_control_id serial NOT NULL,
    parameter text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    range text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_controls_pkey PRIMARY KEY (gear_control_id),
    CONSTRAINT gear_controls_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_links (
    gear_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    gear_id integer NOT NULL,
    CONSTRAINT gear_links_pkey PRIMARY KEY (gear_link_id),
    CONSTRAINT gear_links_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.gear_makers (
    gear_maker_id serial NOT NULL,
    gear_id integer NOT NULL,
    maker_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT gear_makers_pkey PRIMARY KEY (gear_maker_id),
    CONSTRAINT gear_makers_gear_id_fkey FOREIGN KEY (gear_id)
        REFERENCES public.gears (gear_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT gear_makers_maker_id_fkey FOREIGN KEY (maker_id)
        REFERENCES public.makers (maker_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;


\echo
\echo Set owner to gearogs
ALTER TABLE public.gears OWNER to gearogs;
ALTER TABLE public.gear_functions OWNER to gearogs;
ALTER TABLE public.gear_channels OWNER to gearogs;
ALTER TABLE public.gear_dimensions OWNER to gearogs;
ALTER TABLE public.gear_specs OWNER to gearogs;
ALTER TABLE public.gear_indicators OWNER to gearogs;
ALTER TABLE public.gear_connections OWNER to gearogs;
ALTER TABLE public.gear_connection_descriptions OWNER to gearogs;
ALTER TABLE public.gear_controls OWNER to gearogs;
ALTER TABLE public.gear_links OWNER to gearogs;
ALTER TABLE public.gear_makers OWNER to gearogs;

\echo
\echo Extract data from gearogs-gears.json
CREATE TEMP TABLE gears_import_step1(gears json);

\copy gears_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./gearogs-gears.json'

CREATE TEMP TABLE gears_import_step2(
	gear_id int,
    	slug text,
	title text,
        model_number text,
	form_factor text,
	signal_path text,
    	country text,
	date text,
	weight text,
	power_source text,
	frequency_response text,
	dimensions_old text,
	notes text,
	functions json,
	channels json,
	dimensions json,
	specs json,
	indicators json,		
	connections json,
	controls json,
	links json,
	makers json
);

-- Extract single rows
INSERT INTO gears_import_step2(gear_id, slug, title, model_number, form_factor, signal_path, country, date, weight, power_source, frequency_response, dimensions_old, notes, functions, channels, dimensions, specs, indicators, connections, controls, links, makers)
SELECT id, slug, title, model_number, form_factor, signal_path, country_of_manufacture, dates_produced, weight, power_source, frequency_response, dimensions_string, notes, functions, channels_voices_tracks, dimensions, additional_specifications, displays_indicators_meters, connections, controls, external_links, maker FROM gears_import_step1,
json_to_recordset(gears_import_step1.gears::json) AS t(id int, slug text, title text, model_number text, form_factor text, signal_path text, country_of_manufacture text, dates_produced text, weight text, power_source text, frequency_response text, dimensions_string text, notes text, functions json, channels_voices_tracks json, dimensions json, additional_specifications json, displays_indicators_meters json, connections json, controls json, external_links json, maker json);

DROP TABLE gears_import_step1;

\echo
\echo Import to table gears
INSERT INTO gears (gear_id, slug, title, model_number, form_factor, signal_path, country, date, weight, power_source, frequency_response, dimensions_old, notes) 
SELECT gear_id, slug, title, model_number, form_factor, signal_path, country, date, weight, power_source, frequency_response, dimensions_old, notes FROM gears_import_step2;

\echo
\echo Import to table gear_functions
INSERT INTO gear_functions (function, definition, gear_id)
SELECT function,further_definition,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.functions::json) AS t(function text, further_definition text);

\echo
\echo Import to table gear_channels
INSERT INTO gear_channels (type, information, quantity, gear_id)
SELECT type,additional_information,quantity,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.channels::json) AS t(type text, additional_information text, quantity text);

\echo
\echo Import to table gear_dimensions
INSERT INTO gear_dimensions (width, depth, height, notes, gear_id)
SELECT width,depth,height,t.notes,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.dimensions::json) AS t(width numeric, depth numeric, height numeric, notes text);

\echo
\echo Import to table gear_specs
INSERT INTO gear_specs (name,value,information, gear_id)
SELECT parameter_name,parameter_value,additional_information,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.specs::json) AS t(parameter_name text, parameter_value text, additional_information text);

\echo
\echo Import to table gear_indicators
INSERT INTO gear_indicators (indication,type,scale,gear_id)
SELECT indication,type,scale,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.indicators::json) AS t(indication text, type text, scale text);


CREATE TEMP TABLE gears_import_connections(
	gear_connection_id serial NOT NULL,
	gear_id int,
    	name text,
	connector text,
        descriptions json
);

INSERT INTO gears_import_connections (name,connector,descriptions,gear_id)
SELECT name,connector,description,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.connections::json) AS t(name text, connector text, description json);

\echo
\echo Import to table gear_connections
INSERT INTO gear_connections (gear_connection_id,name,connector,gear_id)
SELECT gear_connection_id,name,connector,gear_id FROM gears_import_connections;

\echo
\echo Import to table gear_connection_descriptions
INSERT INTO gear_connection_descriptions (description,gear_connection_id)
SELECT description,gear_connection_id FROM gears_import_connections,
json_array_elements_text(gears_import_connections.descriptions::json) as description;

DROP TABLE gears_import_connections;

\echo
\echo Import to table gear_controls
INSERT INTO gear_controls (parameter,type,range,gear_id)
SELECT parameter,control_type,range,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.controls::json) AS t(parameter text, control_type text, range text);

\echo
\echo Import to table gear_links
INSERT INTO gear_links(url, notes, gear_id)
SELECT url,t.notes,gear_id FROM gears_import_step2,
json_to_recordset(gears_import_step2.links::json) AS t(url text, notes text);

\echo 
\echo Search for broken maker-relations (maker_id is NULL or maker_id not in table makers)
SELECT gears_import_step2.title,gear_id,id,t.title,type,name_variation FROM gears_import_step2,
json_to_recordset(gears_import_step2.makers::json) AS t(id int, title text, type text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT maker_id FROM makers WHERE maker_id=id));

\echo
\echo Import to table gears_makers
INSERT INTO gear_makers (gear_id, maker_id, title, type, name_variation)
SELECT gear_id,id,t.title,type,name_variation FROM gears_import_step2,
json_to_recordset(gears_import_step2.makers::json) AS t(id int, title text, type text, name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT maker_id FROM makers WHERE maker_id=id));

-- remove temporary table
\echo
\echo Clean up
--DROP TABLE gears_import_step2;
\echo
\echo Finished import gears
\echo
