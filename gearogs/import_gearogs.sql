-- WARNING script completely removes the previous database

\echo
\echo STARTED SCRIPT GEAROGS-IMPORT
\echo =============================
\echo

\pset pager off

\echo
\echo Remove old database and user
DROP DATABASE gearogs;
DROP OWNED BY gearogs CASCADE;
DROP USER gearogs;

\echo
\echo Create database gearogs and user gearogs
CREATE DATABASE gearogs;
CREATE USER gearogs WITH PASSWORD 'gearogs';
GRANT ALL PRIVILEGES ON DATABASE gearogs TO gearogs;

\connect gearogs

\i ./import_gearogs_makers.sql
\i ./import_gearogs_gears.sql

\pset pager on

\echo
\echo FINISHED SCRIPT GEAROGS-IMPORT
