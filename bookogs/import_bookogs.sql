-- WARNING script completely removes the previous database

\echo
\echo STARTED SCRIPT BOOKOGS-IMPORT
\echo =============================
\echo

\pset pager off

\echo
\echo Remove old database and user
DROP DATABASE bookogs;
DROP OWNED BY bookogs CASCADE;
DROP USER bookogs;

\echo
\echo Create database bookogs and user bookogs
CREATE DATABASE bookogs;
CREATE USER bookogs WITH PASSWORD 'bookogs';
GRANT ALL PRIVILEGES ON DATABASE bookogs TO bookogs;

\connect bookogs

\i ./import_bookogs_credits.sql
\i ./import_bookogs_works.sql
\i ./import_bookogs_books.sql

\pset pager on

\echo
\echo FINISHED SCRIPT BOOKOGS-IMPORT
