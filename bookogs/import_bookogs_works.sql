\echo
\echo Start import works
\echo ------------------
-- Disabled because database is dropped
--DROP TABLE public.work_credits;
--DROP TABLE public.work_dates;
--DROP TABLE public.work_links;
--DROP TABLE public.work_genres;
--DROP TABLE public.work_languages;
--DROP TABLE public.works;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE work_import_step1;
DROP TABLE work_import_step2;

\echo
\echo Create tables
CREATE TABLE public.works (
    work_id integer NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    location text COLLATE pg_catalog."default",
    notes text COLLATE pg_catalog."default",
    CONSTRAINT works_pkey PRIMARY KEY (work_id)
) TABLESPACE pg_default;

CREATE TABLE public.work_languages (
    work_language_id serial NOT NULL,
    work_id integer NOT NULL,
    language text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT work_languages_pkey PRIMARY KEY (work_language_id),
    --CONSTRAINT work_languages_work_id_language_key UNIQUE (work_id, language),
    CONSTRAINT work_languages_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.work_genres (
    work_genres_id serial NOT NULL,
    work_id integer NOT NULL,
    genre text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT work_genres_pkey PRIMARY KEY (work_genres_id),
    --CONSTRAINT work_genres_work_id_genre_key UNIQUE (work_id, genre),
    CONSTRAINT work_genres_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.work_links (
    work_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    work_id integer NOT NULL,
    CONSTRAINT work_links_pkey PRIMARY KEY (work_link_id),
    CONSTRAINT work_links_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.work_dates (
    work_date_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    type text COLLATE pg_catalog."default",
    work_id integer NOT NULL,
    CONSTRAINT work_dates_pkey PRIMARY KEY (work_date_id),
    CONSTRAINT work_dates_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.work_credits (
    work_credit_id serial,
    work_id integer NOT NULL,
    credit_id integer NOT NULL,
    title text COLLATE pg_catalog."default",
    name_variation text COLLATE pg_catalog."default",
    CONSTRAINT work_credits_pkey PRIMARY KEY (work_credit_id),
    CONSTRAINT work_credits_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT work_credits_work_id_fkey FOREIGN KEY (work_id)
        REFERENCES public.works (work_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to bookogs
ALTER TABLE public.works OWNER to bookogs;
ALTER TABLE public.work_languages OWNER to bookogs;
ALTER TABLE public.work_genres OWNER to bookogs;
ALTER TABLE public.work_links OWNER to bookogs;
ALTER TABLE public.work_dates OWNER to bookogs;
ALTER TABLE public.work_credits OWNER to bookogs;

\echo
\echo Extract data from bookogs-works.json
CREATE TEMP TABLE work_import_step1(work json);

\copy work_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./bookogs-works.json'

CREATE TEMP TABLE work_import_step2(
	work_id int,
	title text,
	slug text,
	languages json,
	credits json,
	location text,
	genre json,
	links json,
	dates json,
	notes text
);

-- Extract single rows
INSERT INTO work_import_step2(work_id,title,slug,languages,credits,location,genre,links,dates,notes) 
SELECT id,title,slug,languages,credits,location,genre,external_links,dates,notes FROM work_import_step1,
json_to_recordset(work_import_step1.work::json) AS t(id int,title text, slug text, languages json, credits json, location text, genre json, external_links json, dates json, notes text);

DROP TABLE work_import_step1;

\echo
\echo Import to table works
INSERT INTO works (work_id, title, slug, location) 
SELECT work_id, title, slug, location FROM work_import_step2;

\echo
\echo Import to table work_languages
INSERT INTO work_languages (work_id, language) 
SELECT work_id,lang FROM work_import_step2,
json_array_elements_text(work_import_step2.languages::json) as lang;

\echo
\echo Import to table work_genres
INSERT INTO work_genres (work_id, genre)
SELECT work_id,t.genre FROM work_import_step2,
json_to_recordset(work_import_step2.genre::json) AS t(genre text);

\echo
\echo Import to table work_links
INSERT INTO work_links (url, notes, work_id)
SELECT url,t.notes,work_id FROM work_import_step2,
json_to_recordset(work_import_step2.links::json) AS t(url text, notes text);

\echo
\echo Import to table work_dates
INSERT INTO work_dates (date, type, work_id)
SELECT date,type,work_id FROM work_import_step2,
json_to_recordset(work_import_step2.dates::json) AS t(date text, type text);

\echo 
\echo Search for broken credit-relations (credit_id is NULL or credit_id not in table credits)
SELECT work_import_step2.title,work_id,id,t.title,name_variation FROM work_import_step2,
json_to_recordset(work_import_step2.credits::json) AS t(id int, title text, name_variation text)
WHERE (id IS NULL) OR (id NOT IN (SELECT credit_id FROM credits WHERE credit_id=id));

\echo
\echo Import to table work_credits
INSERT INTO work_credits (work_id, credit_id, title, name_variation)
SELECT work_id,id,t.title,name_variation FROM work_import_step2,
json_to_recordset(work_import_step2.credits::json) AS t(id int, title text,name_variation text)
WHERE (id IS NOT NULL) AND (id IN (SELECT credit_id FROM credits));

-- remove temporary table
\echo
\echo Clean up
DROP TABLE work_import_step2;
\echo
\echo Finished import works
\echo
