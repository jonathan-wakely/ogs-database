\echo 
\echo Start import series
\echo -------------------

-- Disabled because database is dropped
--DROP TABLE public.series_links;
--DROP TABLE public.series_events;
--DROP TABLE public.series;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE series_import_step1;
DROP TABLE series_import_step2;

\echo
\echo Create tables
CREATE TABLE public.series (
    series_id serial NOT NULL,
    title text COLLATE pg_catalog."default",
    slug text COLLATE pg_catalog."default" NOT NULL,
    profile text COLLATE pg_catalog."default",
    CONSTRAINT series_pkey PRIMARY KEY (series_id),
    CONSTRAINT series_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.series_events (
    series_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    place text COLLATE pg_catalog."default",
    event text COLLATE pg_catalog."default",
    series_id integer NOT NULL,
    CONSTRAINT series_events_pkey PRIMARY KEY (series_event_id),
    CONSTRAINT series_events_serie_id_fkey FOREIGN KEY (series_id)
        REFERENCES public.series (series_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.series_links (
    series_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    series_id integer NOT NULL,
    CONSTRAINT series_links_pkey PRIMARY KEY (series_link_id),
    CONSTRAINT series_links_series_id_fkey FOREIGN KEY (series_id)
        REFERENCES public.series (series_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to comicogs
ALTER TABLE public.series OWNER to comicogs;
ALTER TABLE public.series_events OWNER to comicogs;
ALTER TABLE public.series_links OWNER to comicogs;

\echo
\echo Extract data from comicogs-series.json
CREATE TEMP TABLE series_import_step1(series json);

\copy series_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./comicogs-series.json'

CREATE TEMP TABLE series_import_step2 (series_id integer, title text, profile text, slug text, events json, links json);

-- Extract single rows
INSERT INTO series_import_step2 (series_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM series_import_step1,
json_to_recordset(series_import_step1.series::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE series_import_step1;

\echo
\echo Import to table series
INSERT INTO series (series_id, title, slug, profile)
SELECT series_id, title, slug, profile FROM series_import_step2;

\echo
\echo Import to table series_events
INSERT INTO series_events (date, place, event, series_id)
SELECT date,place,event,series_id FROM series_import_step2,
json_to_recordset(series_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table series_links
INSERT INTO series_links (url, notes, series_id)
SELECT url,notes,series_id FROM series_import_step2,
json_to_recordset(series_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE series_import_step2;
\echo
\echo Finished import series
\echo

