\echo 
\echo Start import publishers
\echo -----------------------

-- Disabled because database is dropped
--DROP TABLE public.publisher_links;
--DROP TABLE public.publisher_events;
--DROP TABLE public.publishers;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE publisher_import_step1;
DROP TABLE publisher_import_step2;

\echo
\echo Create tables
CREATE TABLE public.publishers (
    publisher_id serial NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    profile text COLLATE pg_catalog."default",
    CONSTRAINT publishers_pkey PRIMARY KEY (publisher_id),
    CONSTRAINT publishers_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.publisher_events (
    publisher_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    place text COLLATE pg_catalog."default",
    event text COLLATE pg_catalog."default",
    publisher_id integer NOT NULL,
    CONSTRAINT publisher_events_pkey PRIMARY KEY (publisher_event_id),
    CONSTRAINT publisher_events_publisher_id_fkey FOREIGN KEY (publisher_id)
        REFERENCES public.publishers (publisher_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.publisher_links (
    publisher_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    publisher_id integer NOT NULL,
    CONSTRAINT publisher_links_pkey PRIMARY KEY (publisher_link_id),
    CONSTRAINT publisher_links_publisher_id_fkey FOREIGN KEY (publisher_id)
        REFERENCES public.publishers (publisher_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to comicogs
ALTER TABLE public.publishers OWNER to comicogs;
ALTER TABLE public.publisher_events OWNER to comicogs;
ALTER TABLE public.publisher_links OWNER to comicogs;

\echo
\echo Extract data from comicogs-publishers.json
CREATE TEMP TABLE publisher_import_step1(publisher json);

\copy publisher_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./comicogs-publishers.json'

CREATE TEMP TABLE publisher_import_step2 (
	publisher_id integer,
	title text,
	profile text,
	slug text,
	events json,
	links json
);

-- Extract single rows
INSERT INTO publisher_import_step2 (publisher_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM publisher_import_step1,
json_to_recordset(publisher_import_step1.publisher::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE publisher_import_step1;

\echo
\echo Import to table publishers
INSERT INTO publishers (publisher_id, title, slug, profile)
SELECT publisher_id, title, slug, profile FROM publisher_import_step2;

\echo
\echo Import to table publisher_events
INSERT INTO publisher_events (date, place, event, publisher_id)
SELECT date,place,event,publisher_id FROM publisher_import_step2,
json_to_recordset(publisher_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table publisher_links
INSERT INTO publisher_links (url, notes, publisher_id)
SELECT url,notes,publisher_id FROM publisher_import_step2,
json_to_recordset(publisher_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE publisher_import_step2;
\echo
\echo Finished import publishers
\echo

