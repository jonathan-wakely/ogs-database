-- WARNING script completely removes the previous database

\echo
\echo STARTED SCRIPT COMICOGS-IMPORT
\echo ==============================
\echo

\pset pager off

\echo
\echo Remove old database and user
DROP DATABASE comicogs;
DROP OWNED BY comicogs CASCADE;
DROP USER comicogs;

\echo
\echo Create database comicogs and user comicogs
CREATE DATABASE comicogs;
CREATE USER comicogs WITH PASSWORD 'comicogs';
GRANT ALL PRIVILEGES ON DATABASE comicogs TO comicogs;

\connect comicogs

\i ./import_comicogs_credits.sql
\i ./import_comicogs_publishers.sql
\i ./import_comicogs_series.sql
\i ./import_comicogs_comics.sql

\pset pager on

\echo
\echo FINISHED SCRIPT COMICOGS-IMPORT
