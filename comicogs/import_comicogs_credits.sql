\echo 
\echo Start import credits
\echo --------------------

-- Disabled because database is dropped
--DROP TABLE public.credit_links;
--DROP TABLE public.credit_events;
--DROP TABLE public.credits;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE credit_import_step1;
DROP TABLE credit_import_step2;

\echo
\echo Create tables
CREATE TABLE public.credits (
    credit_id serial NOT NULL,
    slug text COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default",
    profile text COLLATE pg_catalog."default",
    CONSTRAINT credits_pkey PRIMARY KEY (credit_id),
    CONSTRAINT credits_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.credit_events (
    credit_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    event text COLLATE pg_catalog."default",
    place text COLLATE pg_catalog."default",
    credit_id integer NOT NULL,
    CONSTRAINT credit_events_pkey PRIMARY KEY (credit_event_id),
    CONSTRAINT credit_events_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.credit_links (
    credit_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    credit_id integer NOT NULL,
    CONSTRAINT credit_links_pkey PRIMARY KEY (credit_link_id),
    CONSTRAINT credit_links_credit_id_fkey FOREIGN KEY (credit_id)
        REFERENCES public.credits (credit_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to comicogs
ALTER TABLE public.credits OWNER to comicogs;
ALTER TABLE public.credit_events OWNER to comicogs;
ALTER TABLE public.credit_links OWNER to comicogs;

\echo
\echo Extract data from comicogs-credits.json
CREATE TEMP TABLE credit_import_step1(credit json);

\copy credit_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./comicogs-credits.json'

CREATE TEMP TABLE credit_import_step2 (
	credit_id integer,
	title text,
	profile text,
	slug text,
	events json,
	links json
);

-- Extract single rows
INSERT INTO credit_import_step2 (credit_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM credit_import_step1,
json_to_recordset(credit_import_step1.credit::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE credit_import_step1;

\echo
\echo Import to table credits
INSERT INTO credits (credit_id, title, slug, profile)
SELECT credit_id, title, slug, profile FROM credit_import_step2;

\echo
\echo Import to table credit_events
INSERT INTO credit_events (date, place, event, credit_id)
SELECT date,place,event,credit_id FROM credit_import_step2,
json_to_recordset(credit_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table credit_links
INSERT INTO credit_links (url, notes, credit_id)
SELECT url,notes,credit_id FROM credit_import_step2,
json_to_recordset(credit_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE credit_import_step2;
\echo
\echo Finished import credits
\echo

