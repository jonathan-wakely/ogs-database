## OGS-Database

This project contains scripts to import the *ogs-json files to postgresql databases. To use the scripts install postgresql and download the json-files from <http://data.discogslabs.com/>.

### Comicogs
1. Download and extract the json files
2. Remove the year and month from the filename: comicogs-credits.json, comicogs-publishers.json, comicogs-series.json and comicogs-comics.json
3. Copy the files to the comicogs-scripts folder
4. Go to the comicogs-scripts folder
5. run the postgresql script 
        `sudo -U postgres psql -f import_comicogs.sql`
6. wait

The script will create a database comicogs, user comicogs (with password comicogs) and imports the json files into the database.

### Bookogs
1. Download and extract the json files
2. Remove the year and month from the filename: bookogs-credits.json, bookogs-works.json and bookogs-books.json
3. Copy the files to the bookogs-scripts folder
4. Go to the bookogs-scripts folder
5. run the postgresql script 
        `sudo -U postgres psql -f import_bookogs.sql`
6. wait

The script will create a database bookogs, user bookogs (with password bookogs) and imports the json files into the database.

### Filmogs
1. Download and extract the json files
2. Remove the year and month from the filename: filmogs-credits.json, filmogs-companies.json, filmogs-films.json and filmogs-releases.json
3. Copy the files to the filmogs-scripts folder
4. Go to the filmogs-scripts folder
5. run the postgresql script 
        `sudo -U postgres psql -f import_filmogs.sql`
6. wait

The script will create a database filmogs, user filmogs (with password filmogs) and imports the json files into the database.

### Posterogs
1. Download and extract the json files
2. Remove the year and month from the filename: posterogs-credits.json, posterogs-subjects.json, posterogs-venues.json and posterogs-posters.json
3. Copy the files to the posterogs-scripts folder
4. Go to the posterogs-scripts folder
5. run the postgresql script 
        `sudo -U postgres psql -f import_posterogs.sql`
6. wait

The script will create a database posterogs, user posterogs (with password posterogs) and imports the json files into the database.

### Gearogs
1. Download and extract the json files
2. Remove the year and month from the filename: gearogs-makers.json and gearogs-gears.json
3. Copy the files to the gearogs-scripts folder
4. Go to the gearogs-scripts folder
5. run the postgresql script 
        `sudo -U postgres psql -f import_gearogs.sql`
6. wait

The script will create a database gearogs, user gearogs (with password gearogs) and imports the json files into the database.