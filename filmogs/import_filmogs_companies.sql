\echo 
\echo Start import companies
\echo ----------------------

-- Disabled because database is dropped
--DROP TABLE public.company_links;
--DROP TABLE public.company_events;
--DROP TABLE public.companies;

-- DROP temporary tables if script was aborted
\echo
\echo Drop temporary tables (errors can be ignored)
DROP TABLE company_import_step1;
DROP TABLE company_import_step2;

\echo
\echo Create tables
CREATE TABLE public.companies (
    company_id serial NOT NULL,
    title text COLLATE pg_catalog."default",
    slug text COLLATE pg_catalog."default" NOT NULL,
    profile text COLLATE pg_catalog."default",
    CONSTRAINT companies_pkey PRIMARY KEY (company_id),
    CONSTRAINT companies_slug_key UNIQUE (slug)
) TABLESPACE pg_default;

CREATE TABLE public.company_events (
    company_event_id serial NOT NULL,
    date text COLLATE pg_catalog."default" NOT NULL,
    place text COLLATE pg_catalog."default",
    event text COLLATE pg_catalog."default",
    company_id integer NOT NULL,
    CONSTRAINT company_events_pkey PRIMARY KEY (company_event_id),
    CONSTRAINT company_events_company_id_fkey FOREIGN KEY (company_id)
        REFERENCES public.companies (company_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

CREATE TABLE public.company_links (
    company_link_id serial NOT NULL,
    url text COLLATE pg_catalog."default" NOT NULL,
    notes text COLLATE pg_catalog."default",
    company_id integer NOT NULL,
    CONSTRAINT company_links_pkey PRIMARY KEY (company_link_id),
    CONSTRAINT company_links_company_id_fkey FOREIGN KEY (company_id)
        REFERENCES public.companies (company_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
) TABLESPACE pg_default;

\echo
\echo Set owner to comicogs
ALTER TABLE public.companies OWNER to filmogs;
ALTER TABLE public.company_events OWNER to filmogs;
ALTER TABLE public.company_links OWNER to filmogs;

\echo
\echo Extract data from comicogs-companies.json
CREATE TEMP TABLE company_import_step1(company json);

\copy company_import_step1 FROM program 'sed -e ''s/\\/\\\\/g'' ./filmogs-companies.json'

CREATE TEMP TABLE company_import_step2 (
	company_id integer,
	title text,
	profile text,
	slug text,
	events json,
	links json
);

-- Extract single rows
INSERT INTO company_import_step2 (company_id, title, profile, slug, events, links)
SELECT id,title,profile,slug,events,external_links FROM company_import_step1,
json_to_recordset(company_import_step1.company::json) AS t(id int, title text, profile text, slug text, events json, external_links json);

DROP TABLE company_import_step1;

\echo
\echo Import to table companies
INSERT INTO companies (company_id, title, slug, profile)
SELECT company_id, title, slug, profile FROM company_import_step2;

\echo
\echo Import to table company_events
INSERT INTO company_events (date, place, event, company_id)
SELECT date,place,event,company_id FROM company_import_step2,
json_to_recordset(company_import_step2.events::json) AS t(date text, place text, event text);

\echo
\echo Import to table company_links
INSERT INTO company_links (url, notes, company_id)
SELECT url,notes,company_id FROM company_import_step2,
json_to_recordset(company_import_step2.links::json) AS t(url text, notes text);

-- remove temporary table
\echo
\echo Clean up
DROP TABLE company_import_step2;
\echo
\echo Finished import companies
\echo

